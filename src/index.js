import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter,Route,Switch} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

//COMPONENTS
import Login from './components/login/login';
import EvaluateRequest from './components/evaluate/evaluate';
import Submit from './components/submit/submit';

const App = () => {
    return(
       <BrowserRouter>
       <Switch>
           <Route path = "/" exact component={Login}/>
           <Route path = "/evaluate" component={EvaluateRequest}/>
           <Route path = "/submit" component={Submit} />
       </Switch>
       
       </BrowserRouter>
    )
}

ReactDOM.render(<App />, document.getElementById('root'));

