import React from 'react';

import axios from 'axios';

import {Table, Navbar, Nav, NavDropdown} from 'react-bootstrap';

export default class  extends React.Component {
   
    state = {
        requestDetails: []
    }
    
     
    componentDidMount() {
        axios.get('http://localhost:4000/api/request/showall')
        .then(res => {
            console.log("obtained response -----", res)
            const requestDetails = res.data;
            this.setState({ requestDetails });
        })
    }
    renderTableData() {
        return this.state.requestDetails.map((request,index) =>{
            const { _id, reqTitle, reqDescription, submittedBy} = request;
            return (
                <tr key={_id}>
                    <td>{_id}</td>
                    <td>{reqTitle}</td>
                    <td>{reqDescription}</td>
                    <td>{submittedBy}</td>
                </tr>
            )
        }

        )
    }

    render() {
        return (
            <div>
           <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Navbar.Brand href="#home">Home</Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="#features">Features</Nav.Link>
      <Nav.Link href="#pricing">Pricing</Nav.Link>
      <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    <Nav>
      <Nav.Link href="#deets">More deets</Nav.Link>
      <Nav.Link eventKey={2} href="#memes">
        Logout
      </Nav.Link>
    </Nav>
  </Navbar.Collapse>
</Navbar>
            <Table Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>id</th>
                <th>Request Title</th>
                <th>Description </th>
                <th>SubmittedBy</th>
              </tr>
            </thead>
            <tbody>
              
            {this.renderTableData()}
            </tbody>
          </Table>
          </div>
        )
    }
}