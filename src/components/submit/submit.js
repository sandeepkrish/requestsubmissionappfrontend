import React, {Component} from 'react';
import {Form,FormCheck, Button,Container,Row,Col} from 'react-bootstrap';
import axios from 'axios';

 class Submit extends Component {

     constructor() {

         super();

         this.state = {
            
             selectedFile: '',
             reqTitle: '',
             reqDescription: '',
             submittedBy: ''
         };

     }
         onChange = (e) => {
             switch (e.target.name) {
                 case 'selectedFile':
                     this.setState({selectedFile: e.target.files[0]});
                     break;
                     default:
                         this.setState({ [e.target.name]: e.target.value})
             }

         }

         onSubmit = (e) => {
             e.preventDefault();
             const reqDetails = {

                reqTitle:this.state.reqTitle,
                reqDescription:this.state.reqDescription,
                submittedBy:this.state.submittedBy,
                selectedFile:this.state.selectedFile

             }
        
             axios.post('http://localhost:4000/api/request',{reqDetails})
            .then(res => {
                console.log(res.data)

            })
        }
    render() {
        const { reqTitle,reqDescription,submittedBy, selectedFile } = this.state;
        return(
            <Container>
                <Row>
                    <Col></Col>
                    <Col xs={6}>
                    <Form onSubmit={this.onSubmit}>
                <Form.Group controlId="reqTitle">
                    <Form.Label>Request Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter Title" 
                    onChange={this.onChange}
                    value={reqTitle}/>
                </Form.Group>

                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Description</Form.Label>
                    <Form.Control as="textarea" rows="3"
                    onChange={this.onChange} 
                    value={reqDescription}/>
                </Form.Group>

                <Form.Group controlId="submittedBy">
                    <Form.Label>Submitted By</Form.Label>
                    <Form.Control type="text" placeholder="Enter Username"
                    onChange={this.onChange} 
                    value={submittedBy}/>
                </Form.Group>
                <input
                        type="file"
                        name="selectedFile"
                        value={selectedFile}
                        onChange={this.onChange}
                />
                <br/><br/>
                <Button variant="success" type="submit">Submit</Button>

            </Form>
                    </Col>
                    <Col></Col>
                </Row>
            </Container>
                
           
        )
    }
}


export default Submit;