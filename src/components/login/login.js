import React, {useState, Component} from "react";
import {Button, FormGroup, FormControl, FormLabel } from "react-bootstrap";
import './login.css';
import Axios from "axios";

class Login extends Component {
  
     state={
          username:'',
          password:'',
          errorMessage:''
      }

      handleUsernameChange = (event) =>{
          this.setState({
              username:event.target.value
          })
      }

      handlePasswordChange = (event) => {
          this.setState({
              password:event.target.value
          })
      }
  

    

     handleSubmit = (event) =>{
        event.preventDefault();
        var historyRoute = this.props.history
        var apiBaseUrl = "http://localhost:4000/api/";
        console.log(this.state)
        var userCredentials = {
        "username":this.state.username,
        "password":this.state.password
        }
        Axios.post(apiBaseUrl+'login',userCredentials,{
            headers:{
                'Content-Type':'application/json'
            }
        })
        .then( function (response) {
            console.log(response);
            var token = response.data.token;
            var role = response.data.role;
            localStorage.setItem("token",token);
            localStorage.setItem("role",role);

            if(localStorage.getItem("role") === "submitter"){
                console.log("Login Suceesfull");
                alert("successfull LOgin");
                historyRoute.push('/submit');
            }
            else if (localStorage.getItem("role") === "approver"){
                console.log("Username password mismatch");
                alert("Username password mismatch");
                historyRoute.push('/approve');
            }
            else if (localStorage.getItem("role") === "evaluator"){
                
                historyRoute.push('/evaluate');
            }
            else{
                console.log("User doesnot exist");
                alert("User not found");
                historyRoute.push('/error');
            }
        })
        .catch(function (error)
        {
            console.log(error);
            this.setState({
                errorMessage:error.response.data.message
            })
        });
    }
    render(){
    return (
        <div className="Login">
            <form onSubmit={this.handleSubmit}>
                <FormGroup controlId="username" bsSize="large">
                    <FormLabel>Username</FormLabel>
                    <FormControl
                     autoFocus
                     type="text"
                     onChange={this.handleUsernameChange}
                     value={this.state.username}
                    />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <FormLabel>Password</FormLabel>
                    <FormControl
                     autoFocus
                     type="password"
                     value={this.state.password}
                     onChange={this.handlePasswordChange}
                    />
                </FormGroup>
                <Button block bsSize="large"  type="submit">
                    Login
                </Button>
                <div className="error">{this.state.errorMessage}</div>
            </form>
        </div>
    );
}
}

export default Login;